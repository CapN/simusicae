const httpServer = require("http").createServer();
const io = require("socket.io")(httpServer, {
    cors: {
        origin: '*',
    }
});
let rooms = []

io.on("connection", (socket) => {
    console.info("client connecté");
    //test des ws
    socket.on("message", data => {
        console.log("message", data)
        io.to(rooms).emit('welcom', data)
    })
    //creer ou rejoind un room
    socket.on('getInRoom', roomName => {
        console.info("getInRoom", roomName)
        socket.join(roomName)
        socket.room = roomName
        let groupe = {
            name: roomName,
            instruments: []
        }
        rooms.push(groupe)
        io.to(roomName).emit('welcom', 'BIENVENUE DANS LA ROOM')
        io.emit('newRoom', rooms)
    })
    socket.on('joinRoom', roomName => {
        console.info("join", roomName)
        socket.join(roomName)
        socket.room = roomName
        rooms.forEach((g) => {
            if (g.name === roomName) {
                io.to(roomName).emit('pickedInstrument', g.instruments)
            }
        })
        // io.to(roomName).emit('welcom', 'BIENVENUE DANS LA ROOM')
    })

    //Add Nickname
    socket.on('addNickname', nickName => {
        console.info("new member", nickName)
        socket.nickName = nickName

        // io.to(roomName).emit('welcom', 'BIENVENUE DANS LA ROOM')
    })

    //renvoie la listes des salons actifs
    socket.on("getListRoom", (callback) => {
        console.log(rooms)
        let groupsNameListe = []
        rooms.forEach((g) => {
            groupsNameListe.push(g.name)
        })
        callback({
            rooms: groupsNameListe
        })
    })

    //attribut l'instrument choisis au musicient
    socket.on("instrument", instrument => {
        // let room = socket.room
        rooms.forEach((g) => {
            let room = socket.room
            console.log(room,"instrument")
            if (g.name === room) {
                if (socket.instrument) {
                    console.log("instrument trouvé")
                    g.instruments = g.instruments.filter(item => item !== socket.instrument)
                    console.log('instruments', g.instruments)
                }
                socket.instrument = instrument
                g.instruments.push(instrument)
                io.to(room).emit('pickedInstrument', g.instruments)
            }
        })

        console.log(rooms)
    })
});

httpServer.listen(3000);